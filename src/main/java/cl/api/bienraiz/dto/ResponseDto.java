package cl.api.bienraiz.dto;

import cl.api.bienraiz.models.PjProyecto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Data
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDto {

    private List<BienRaizDto> bienRaiz;
    private Set<PjProyecto> proyecto;

}

package cl.api.bienraiz.dto;

import cl.api.bienraiz.models.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Data
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BienRaizDto {

    private Long id;

    private Set<BrDatoAlzamiento> datosAlzamiento;

    private Set<BrAdicional> brAdicionales;

    private PjProyecto pjProyecto;

    private Long pjProyectoId;

    private String calle;

    private String numero;

    private Long regionId;

    private Region region;

    private Long ciudadId;

    private Ciudad ciudad;

    private Long comunaId;

    private Comuna comuna;

    private String numDepto;

    private String resto;

    private String numRol;

    private Boolean flagEncadenamiento;

    private TipoBienRaiz tipoBienRaiz;

    private Long estadoBienRaizId;

    private BrEstado estadoBienRaiz;

    private Long tipoPagoInmobiliariaId;

    private TipoPagoInmobiliaria tipoPagoInmobiliaria;

    private Date fechaTitulos;

    private Date fechaGgpp;

    private String ubicacionReferencia;

    private String contactoNombre;

    private String contactoTelefono;

    private String contactoObserv;

    private String numSolicTasacion;

    private Date fechaTasacion;

    private Long tasadorId;

    private Tasador tasador;

    private Long estadoTasacionId;

    private EstadoTasacion estadoTasacion;

    private Boolean mercadoObjeto;

    private BigDecimal superfTerreno;

    private Long tipoConstruccion;

    private Integer anioConstruccion;

    private BigDecimal superfConst;

    private Long unidadId;

    private UnidadMedidaTerreno unidad;

    private Integer indicadorViviendaSocial;

    private Boolean dfl2;

    private Integer numPropiedadDFL2;

    private Long monedaPactadaId;

    private Moneda monedaPactada;

    private BigDecimal valorTasacionuf;

    private Boolean selloVerde;

    private BigDecimal valorLiquidezuf;

    private BigDecimal montoMinAsegurableuf;

    private String observacionTasacion;

    private String numBienraizInst;

    private String numGtiaInst;

    private Long tipoGarantiaId;

    private TipoGarantia tipoGarantia;

    private String afavorde;

    private BigDecimal annoCertifAsigRoles;

    private String certifAsigRoles;

    private BigDecimal certificadoNro;

    private Date fechaPermisoEdif;

    private Date fechaRecepcFinal;

    private Boolean matriceriaHipo;

    private Long municipalidadId;

    private Comuna municipalidad;

    private String numCaratulaCbr;

    private Date fechaEstimadaSalidaCBR;

    private BigDecimal totalMontoUfCresg;

    private BigDecimal totalMontoUfReparo;

    private BigDecimal valorTasacionpesos;

    private Long seguroId;

    private Seguro seguro;

    private Long wfhUsuario;

    private String direccion;

    private Integer estacionamientos;

    private Integer bodegas;

}

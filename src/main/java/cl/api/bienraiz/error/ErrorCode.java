package cl.api.bienraiz.error;

import com.google.common.collect.Maps;
import org.springframework.http.HttpStatus;

import java.util.Map;

import static java.lang.String.format;
import static java.util.Arrays.stream;

public enum ErrorCode {
    // Internal errors
    INTERNAL_ERROR(500,"INTERNAL_ERROR", "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);

    private final int index;
    private final String code;
    private String msg;
    private HttpStatus status;

    private static final Map<String, ErrorCode> nameIndex =
            Maps.newHashMapWithExpectedSize(ErrorCode.values().length);

    static {
        for (ErrorCode suit : ErrorCode.values()) {
            nameIndex.put(suit.getCode(), suit);
        }
    }

    ErrorCode(int index, String code, String msg, HttpStatus status) {
        this.index = index;
        this.code = code;
        this.msg = msg;
        this.status = status;
    }

    public static ErrorCode lookupByName(String name) {
        return nameIndex.get(name);
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public int getIndex(){
        return index;
    }

    /**
     * Whether this error code is in the Error series {@link Series#VALIDATION VALIDATION}.
     * This is a shortcut for checking the value of {@link #series()}.
     */
    public boolean is1xxValidation() {
        return Series.VALIDATION.equals(series());
    }

    /**
     * Whether this error code is in the Error series {@link Series#SECURITY SECURITY}.
     * This is a shortcut for checking the value of {@link #series()}.
     */
    public boolean is2xxSecurity() {
        return Series.SECURITY.equals(series());
    }

    /**
     * Whether this error code is in the Error series {@link Series#BACKEND BACKEND}.
     * This is a shortcut for checking the value of {@link #series()}.
     */
    public boolean is3xxBackEnd() {
        return Series.BACKEND.equals(series());
    }

    /**
     * Returns the Error Code series of this error code.
     *
     * @see Series
     */
    public Series series() {
        return Series.valueOf(this);
    }

    /**
     * Enumeration of error codes series.
     * <p>Retrievable via {@link ErrorCode#series()}.
     */
    public enum Series {
        VALIDATION(1),
        SECURITY(2),
        BACKEND(3);

        private final int value;

        Series(int value) {
            this.value = value;
        }

        public static Series valueOf(int status) {
            int seriesCode = status / 100;
            return stream(values())
                    .filter(series -> series.value == seriesCode)
                    .findAny()
                    .orElseThrow(() -> new IllegalArgumentException(format("No matching constant for '%s'", status)));
        }

        public static Series valueOf(ErrorCode code) {
            return valueOf(code.index);
        }

        /**
         * Return the integer value of this status series. Ranges from 1 to 3.
         */
        public int value() {
            return this.value;
        }
    }
}

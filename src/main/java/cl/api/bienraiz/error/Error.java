package cl.api.bienraiz.error;

import java.io.Serializable;

public class Error implements Serializable {
    private static final long serialVersionUID = -7515187141251150970L;
    private int index;
    private ErrorCode code;
    private String detail;

    public Error() {
    }

    public Error(int index, ErrorCode code, String detail) {
        this.index = index;
        this.code = code;
        this.detail = detail;
    }

    /**
     * Build an error based on the given code and detail.
     *
     * @param code   The code of the error.
     * @param detail The detail of the error.
     * @return An error object populated with the specified code and detail.
     */
    public static Error err(int index, ErrorCode code, String detail) {
        return new Error(index, code, detail);
    }

    public ErrorCode getCode() {
        return code;
    }

    public void setCode(ErrorCode code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}


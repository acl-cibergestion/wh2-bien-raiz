package cl.api.bienraiz.exceptions;

import cl.api.bienraiz.error.ErrorCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomException extends Exception{

    private static final long serialVersionUID = 7718828512143293558L;
    private final ErrorCode errorCode;
    private final int index;
    private final String detail;
    private final String message;
    private final String param;

    public CustomException(ErrorCode errorCode){
        this.errorCode = errorCode;
        this.index = errorCode.getIndex();
        this.param = "";
        this.detail = errorCode.getMsg();
        this.message = String.valueOf(errorCode.getIndex()).concat(": ").concat(errorCode.getCode());
    }

    public CustomException(ErrorCode errorCode, String param){
        this.errorCode = errorCode;
        this.index = errorCode.getIndex();
        this.param = param;
        this.detail = errorCode.getMsg().concat(param);
        this.message = String.valueOf(errorCode.getIndex()).concat(": ").concat(errorCode.getCode());
    }

}

package cl.api.bienraiz.repositories;

import cl.api.bienraiz.models.BienRaiz;
import cl.api.bienraiz.models.CaBienRaiz;
import cl.api.bienraiz.models.Carpeta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface CaBienRaizRepositorio extends JpaRepository<CaBienRaiz, Long>, JpaSpecificationExecutor<CaBienRaiz> {

	void deleteByCarpetaAndBienRaiz(Carpeta carpeta, BienRaiz bienRaiz);
	Optional<CaBienRaiz> findByBienRaizAndAndCarpeta(BienRaiz bienRaiz, Carpeta carpeta);
}

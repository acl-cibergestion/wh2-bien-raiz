package cl.api.bienraiz.repositories;

import cl.api.bienraiz.models.PjProyecto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PjProyectoRepositorio extends JpaRepository<PjProyecto, Long>, JpaSpecificationExecutor<PjProyecto> {
}

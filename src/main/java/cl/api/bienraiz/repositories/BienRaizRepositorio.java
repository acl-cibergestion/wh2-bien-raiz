package cl.api.bienraiz.repositories;

import cl.api.bienraiz.models.BienRaiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BienRaizRepositorio extends JpaRepository<BienRaiz, Long>, JpaSpecificationExecutor<BienRaiz> {
}

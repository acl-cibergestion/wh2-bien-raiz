package cl.api.bienraiz.repositories;

import cl.api.bienraiz.models.Carpeta;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CarpetaRepositorio extends JpaRepository<Carpeta, Long> {
	
}

package cl.api.bienraiz.repositories;

import cl.api.bienraiz.models.BienRaiz;
import cl.api.bienraiz.models.BrDatoAlzamiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface BrDatoAlzamientoRepositorio extends JpaRepository<BrDatoAlzamiento, Long>, JpaSpecificationExecutor<BrDatoAlzamiento> {
    void deleteByBienRaiz(BienRaiz bienRaiz);
    List<BrDatoAlzamiento> findAllByBienRaiz(BienRaiz bienRaiz);
}

package cl.api.bienraiz.repositories;

import cl.api.bienraiz.models.BienRaiz;
import cl.api.bienraiz.models.BrAdicional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface BrAdicionalRepositorio extends JpaRepository<BrAdicional, Long>, JpaSpecificationExecutor<BrAdicional> {
    void deleteByBienRaiz(BienRaiz bienRaiz);
    List<BrAdicional> findAllByBienRaiz(BienRaiz bienRaiz);
}

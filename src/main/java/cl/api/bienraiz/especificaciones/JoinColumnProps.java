package cl.api.bienraiz.especificaciones;

public class JoinColumnProps {
    private String joinColumnName;
    private SearchFilter searchFilter;

    public JoinColumnProps(String joinColumnName, SearchFilter searchFilter) {
        this.joinColumnName = joinColumnName;
        this.searchFilter = searchFilter;
    }

    public String getJoinColumnName() {
        return joinColumnName;
    }

    public void setJoinColumnName(String joinColumnName) {
        this.joinColumnName = joinColumnName;
    }

    public SearchFilter getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(SearchFilter searchFilter) {
        this.searchFilter = searchFilter;
    }

}

package cl.api.bienraiz.controllers;

import cl.api.bienraiz.dto.ResponseDto;
import cl.api.bienraiz.exceptions.CustomException;
import cl.api.bienraiz.models.BienRaiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import cl.api.bienraiz.services.BienRaizService;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/bienRaiz")
public class BienRaizController {

    @Autowired
    BienRaizService bienRaizService;

    @PostMapping("/save/{carpetaId}/{usuarioId}")
    public boolean save(@RequestBody List<BienRaiz> bienRaizList, @PathVariable (value = "carpetaId") Long carpetaId,  @PathVariable (value = "usuarioId") Long usuarioId){
        return bienRaizService.save(bienRaizList, carpetaId, usuarioId);
    }

    @PostMapping(value = "/findBienRaiz")
    public ResponseEntity<List<BienRaiz>> getAllBienRaiz(@RequestBody Map<String, Map<String, String>> request) throws CustomException {
        var listadoBienesRaices =  bienRaizService.findBienRaiz(request);
        return new ResponseEntity<>(listadoBienesRaices, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteBienRaiz (@PathVariable (value = "id") Long id){
        return bienRaizService.deleteBienRaizCarpeta(id);
    }

    @PostMapping("/edit/{carpetaId}/{usuarioId}")
    public boolean edit(@RequestBody List<BienRaiz> bienRaizList, @PathVariable (value = "carpetaId") Long carpetaId,  @PathVariable (value = "usuarioId") Long usuarioId){
        return bienRaizService.edit(bienRaizList, carpetaId, usuarioId);
    }

    @PostMapping(value = "/findBienRaizProyecto")
    public ResponseEntity<ResponseDto> getBienRaizProyecto(@RequestBody Map<String, Map<String, String>> request) throws CustomException {
        var listadoBienesRaices =  bienRaizService.findByFolder(request);
        return new ResponseEntity<>(listadoBienesRaices, HttpStatus.OK);
    }
}

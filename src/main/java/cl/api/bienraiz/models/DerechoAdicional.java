package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_DERECHOS_ADICIONAL database table.
 *
 */
@Entity
@Table(name = "CON_DERECHOS_ADICIONAL", schema = Constant.DB_SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_DERECHOS_ADICIONAL_GEN", sequenceName = "BD_SEC_ID_DERECHOS_ADICIONAL")
@Getter
@Setter
@NoArgsConstructor
public class DerechoAdicional implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_DERECHOS_ADICIONAL_GEN")
    private Long id;

    private String descripcion;

    private Boolean activo;

}

package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistent class for the WFH_BR_ADICIONALES database table.
 *
 */
@Entity
@Table(name = "WFH_CARPETAS", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Carpeta implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private Long id;

    //bi-directional many-to-one association to BienRaiz
    @OneToMany(mappedBy = "carpeta")
    private Set<CaBienRaiz> caBienRaiz;

}

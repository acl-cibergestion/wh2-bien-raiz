package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_INSTITUCIONES database table.
 *
 */
@Entity
@Table(name = "CON_INSTITUCIONES", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Institucion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;
    @OneToOne
    @JoinColumn(name = "TIPO_INSTITUCION_ID")
    private TipoInstitucion tipoInstitucion;
    private Boolean activo;
    private String codigo;
}

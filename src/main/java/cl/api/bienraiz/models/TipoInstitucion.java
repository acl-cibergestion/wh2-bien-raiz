package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_TIPOS_INSTITUCION database table.
 *
 */
@Entity
@Table(name = "CON_TIPOS_INSTITUCION", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class TipoInstitucion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    private String nombre;
    private Boolean activo;
}

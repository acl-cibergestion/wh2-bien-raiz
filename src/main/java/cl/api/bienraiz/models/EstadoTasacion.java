package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_ESTADOS_TASACION database table.
 *
 */
@Entity
@Table(name = "CON_ESTADOS_TASACION", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class EstadoTasacion implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    private String nombre;
    private Boolean activo;

}

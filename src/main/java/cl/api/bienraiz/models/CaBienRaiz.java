package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "WFH_CA_BIENES_RAICES", schema = Constant.DB_SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_CA_BIEN_RAIZ_GEN", sequenceName = "WFC_SEC_ID_CA_BIEN_RAIZ")
@Getter
@Setter
@NoArgsConstructor
public class CaBienRaiz extends MyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_CA_BIEN_RAIZ_GEN")
    private Long id;
	
	@ManyToOne
    @JoinColumn(name = "carpeta_id")
    private Carpeta carpeta;

    @ManyToOne
    @JoinColumn(name = "bien_raiz_id")
    private BienRaiz bienRaiz;

}

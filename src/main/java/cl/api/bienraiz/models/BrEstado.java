package cl.api.bienraiz.models;


import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the WFH_BR_ESTADOS database table.
 * 
 */
@Entity
@Table(name="WFH_BR_ESTADOS", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class BrEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	private String descripcion;


}
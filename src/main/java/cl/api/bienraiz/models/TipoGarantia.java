package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_TIPOS_GARANTIA database table.
 *
 */
@Entity
@Table(name = "CON_TIPOS_GARANTIA", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class TipoGarantia implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String descripcion;

    private Boolean activo;


}

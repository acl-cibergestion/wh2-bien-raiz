package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_MONEDAS database table.
 *
 */
@Entity
@Table(name = "CON_MONEDAS", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Moneda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "CODIGO_ISO")
    private String codigoIso;

    private String nombre;

    private Boolean activo;

}

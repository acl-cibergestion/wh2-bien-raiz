package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the WFH_BR_ADICIONALES database table.
 *
 */
@Entity
@Table(name = "WFH_BR_ADICIONALES", schema = Constant.DB_SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_BR_ADICIONALES_GEN", sequenceName = "WFC_SEC_ID_BR_ADICIONALES")
@Getter
@Setter
@NoArgsConstructor
public class BrAdicional extends MyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_BR_ADICIONALES_GEN")
    private Long id;

    //bi-directional many-to-one association to BienRaiz
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BIEN_RAIZ_ID")
    @JsonIgnore
    private BienRaiz bienRaiz;

    @Column(name = "TIPO_ADICIONAL_ID", precision = 8)
    private Long tipoAdicional;

    @Column(name = "NUMERO")
    private String numero;

    @Column(name = "ROL")
    private String rol;

    @Column(name = "DERECHOS_ID")
    private Long derechoAdicionalId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DERECHOS_ID", updatable = false, insertable = false)
    private DerechoAdicional derechoAdicional;

}

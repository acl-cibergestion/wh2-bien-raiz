package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "CON_CIUDADES", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Ciudad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "ESTADO_CAMBIO_COD")
    private BigDecimal estadoCambioCod;

    private String nombre;

    @OneToOne
    @JoinColumn(name = "REGION_ID",insertable = false,updatable = false)
    private Region region;

    private Boolean activo;
}
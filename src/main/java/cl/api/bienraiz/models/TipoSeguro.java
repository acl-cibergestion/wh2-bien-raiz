package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the CG_MOD_TIPOS_SEGURO database table.
 *
 */
@Entity
@Table(name = "CON_TIPOS_SEGURO", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class TipoSeguro implements Serializable {

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;
    
    @Column(name = "CATEGORIA_ID")
    private Long categoriaSeguroId;

}

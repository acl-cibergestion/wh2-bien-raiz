package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the WFH_BR_DATOS_ALZAMIENTO database table.
 *
 */
@Entity
@Table(name = "WFH_BR_DATOS_ALZAMIENTO", schema = Constant.DB_SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_BR_DATOS_ALZAMIENTO_GEN", sequenceName = "WFC_SEC_ID_BR_DATOS_ALZAMIENTO")
@Getter
@Setter
@NoArgsConstructor
public class BrDatoAlzamiento extends MyEntity implements Serializable{

    private static final long serialVersionUID = 6376210491469299935L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_BR_DATOS_ALZAMIENTO_GEN")
    private Long id;

    //bi-directional many-to-one association to BienRaiz
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BIEN_RAIZ_ID")
    @JsonIgnore
    private BienRaiz bienRaiz;

    @Column(name = "TIPO_ALZAMIENTO")
    private Boolean tipoAlzamiento;

    @Column(name = "SINACOFI")
    private Boolean sinacofi;

    @Column(name = "ACREEDOR_ID")
    private Long bancoAcreedorId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACREEDOR_ID", insertable = false, updatable = false)
    private Banco bancoAcreedor;

    @Column(name = "CARTA_RESGUARDO")
    private Boolean cartaResguardo;

    @Column(name = "CONTACTO")
    private String contacto;

    @Column(name = "SUCURSAL")
    private String sucursal;

    @Column(name = "NISN")
    private String nisn;

    @Column(name = "NNSE")
    private String nnse;

    @Column(name = "TELEFONO")
    private String telefono;

    @Column(name = "BENEFICIARIO")
    private Boolean beneficiario;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIRMA_BANCO_ACREEDOR")
    private Date fechaFirmaBancoAcreedor;

    @Column(name = "FIRMO_BANCO_ACREEDOR")
    private Boolean firmoBancoAcreedor;

    @Column(name = "MONTO_CARTA_RESGUARDO_UF", precision = 9, scale = 2)
    private BigDecimal montoCartaResguardoUf;

    @Column(name = "MONTO_REPARO_UF", precision = 9, scale = 2)
    private BigDecimal montoReparoUf;

    @Column(name = "MOTIVO_NO_FIRMA", precision = 8)
    private Long motivoNoFirma;

}

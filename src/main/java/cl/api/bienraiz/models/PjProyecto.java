package cl.api.bienraiz.models;


import cl.api.bienraiz.utils.Constant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the WFH_PJ_PROYECTOS database table.
 */
@Entity
@Table(name = "WFH_PJ_PROYECTOS", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PjProyecto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_PAGO_ID", updatable=false, insertable = false)
    private TipoPagoInmobiliaria tipoPagoInmobiliaria;

}
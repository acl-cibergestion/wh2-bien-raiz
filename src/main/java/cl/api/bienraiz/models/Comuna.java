package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_COMUNAS database table.
 */
@Entity
@Table(name = "CON_COMUNAS", schema = Constant.DB_SCHEMA)

@Getter
@Setter
@NoArgsConstructor
public class Comuna implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;

    @Column(name = "CBR_DESCRIPCION")
    private String cbrDescripcion;

    @OneToOne
    @JoinColumn(name = "CIUDAD_ID",updatable = false,insertable = false)
    private Ciudad ciudad;
}

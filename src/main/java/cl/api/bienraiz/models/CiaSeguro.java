package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_CIAS_SEGURO database table.
 *
 */
@Entity
@Table(name = "CON_CIAS_SEGURO", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class CiaSeguro implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private Boolean activo;

    private String direccion;

    private String nombre;

    @Column(name = "NOMBRE_CORTO")
    private String nombreCorto;

    private String rut;

    private String telefono;


}

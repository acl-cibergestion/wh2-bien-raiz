package cl.api.bienraiz.models;


import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the CG_MOD_TIPOS_BIEN_RAIZ database table.
 *
 */
@Entity
@Table(name = "CON_TIPOS_BIEN_RAIZ", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class TipoBienRaiz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "PORCENTAJE", precision = 9, scale = 2)
    private BigDecimal porcentaje;

    @Column(name = "TIPO_TASACION_ID", precision = 8)
    private Long tipoTasacion;

    @Column(name = "ACTIVO")
    private Boolean activo;


}

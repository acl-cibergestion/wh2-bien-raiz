package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_TIPOS_PAGO_INMO database table.
 *
 */
@Entity
@Table(name = "CON_TIPOS_PAGO_INMO", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoPagoInmobiliaria implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    @Column(name = "NOMBRE")
    private String nombre;

}

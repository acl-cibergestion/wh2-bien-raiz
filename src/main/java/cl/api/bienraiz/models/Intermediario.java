package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the WFH_INTERMEDIARIOS database table.
 *
 */
@Entity
@Table(name="WFH_INTERMEDIARIOS", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Intermediario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name="ACTIVO")
    private Boolean activo;

    @Column(name="DESCRIPCION")
    private String descripcion;

    @Column(name="RUT")
    private String rut;


}
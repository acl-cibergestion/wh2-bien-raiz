package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_UNIDADES_MEDIDA_TERRENO database table.
 *
 */
@Entity
@Table(name = "CON_UNIDADES_MEDIDA_TERRENO", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class UnidadMedidaTerreno implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    private String descripcion;
    private Boolean activo;

}

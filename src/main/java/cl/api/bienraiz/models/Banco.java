package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_BANCOS database table.
 *
 */
@Entity
@Table(name = "CON_BANCOS", schema = Constant.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Banco implements Serializable {

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;

}

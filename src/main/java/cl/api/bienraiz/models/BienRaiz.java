package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the WFH_BIENES_RAICES database table.
 *
 */
@Entity
@Table(name = "WFH_BIENES_RAICES", schema = Constant.DB_SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_BIENES_RAICES_GEN", sequenceName = "WFC_SEC_ID_BIENES_RAICES")
@Getter
@Setter
@NoArgsConstructor
public class BienRaiz extends MyEntity implements Serializable{

    private static final long serialVersionUID = 6376210491469299941L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_BIENES_RAICES_GEN")
    private Long id;

    //bi-directional many-to-one association to BrDatoAlzamiento
    @OneToMany(mappedBy = "bienRaiz", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<BrDatoAlzamiento> datosAlzamiento;

    //bi-directional many-to-one association to BrAdicional
    @OneToMany(mappedBy = "bienRaiz", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<BrAdicional> brAdicionales;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROYECTO_ID", updatable=false, insertable = false)
    private PjProyecto pjProyecto;

    @Column(name = "PROYECTO_ID")
    private Long pjProyectoId;

    @Column(name = "CALLE")
    private String calle;

    @Column(name = "NUMERO")
    private String numero;

    @Column(name = "REGION_ID")
    private Long regionId;

    @OneToOne
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    private Region region;

    @Column(name = "CIUDAD_ID", precision = 8)
    private Long ciudadId;

    @OneToOne
    @JoinColumn(name = "CIUDAD_ID", insertable = false, updatable = false)
    private Ciudad ciudad;

    @Column(name = "COMUNA_ID", precision = 8)
    private Long comunaId;

    @OneToOne
    @JoinColumn(name = "COMUNA_ID", insertable = false, updatable = false)
    private Comuna comuna;

    @Column(name = "NUM_DEPTO")
    private String numDepto;

    @Column(name = "RESTO")
    private String resto;

    @Column(name = "NUM_ROL")
    private String numRol;

    @Column(name = "FLAG_ENCADENAMIENTO")
    private Boolean flagEncadenamiento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_BIEN_RAIZ_ID")
    private TipoBienRaiz tipoBienRaiz;

    @Column(name = "ESTADO_ID", precision = 8)
    private Long estadoBienRaizId;

    @ManyToOne
    @JoinColumn(name = "ESTADO_ID",insertable = false,updatable = false)
    private BrEstado estadoBienRaiz;

    @Column(name = "TIPO_PAGO_INMOBILIARIA_ID", precision = 8)
    private Long tipoPagoInmobiliariaId;

    @ManyToOne
    @JoinColumn(name = "TIPO_PAGO_INMOBILIARIA_ID",updatable = false,insertable = false)
    private TipoPagoInmobiliaria tipoPagoInmobiliaria;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_TITULOS")
    private Date fechaTitulos;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_GGPP")
    private Date fechaGgpp;

    @Column(name = "UBICACION_REFERENCIA")
    private String ubicacionReferencia;

    @Column(name = "CONTACTO_NOMBRE")
    private String contactoNombre;

    @Column(name = "CONTACTO_TELEFONO")
    private String contactoTelefono;

    @Column(name = "CONTACTO_OBSERV")
    private String contactoObserv;

    @Column(name = "NUM_SOLIC_TASACION")
    private String numSolicTasacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_TASACION")
    private Date fechaTasacion;

    @Column(name = "TASADOR_ID", precision = 8)
    private Long tasadorId;

    @ManyToOne
    @JoinColumn(name = "TASADOR_ID",insertable = false,updatable = false)
    private Tasador tasador;

    @Column(name = "ESTADO_TASACION_ID", precision = 8)
    private Long estadoTasacionId;

    @ManyToOne
    @JoinColumn(name = "ESTADO_TASACION_ID",updatable = false,insertable = false)
    private EstadoTasacion estadoTasacion;

    @Column(name = "MERCADO_OBJETO")
    private Boolean mercadoObjeto;

    @Column(name = "SUPERF_TERRENO", precision = 10, scale = 4)
    private BigDecimal superfTerreno;

    @Column(name = "TIPO_CONSTRUCCION_ID", precision = 8)
    private Long tipoConstruccion;

    @Column(name = "ANO_CONST", precision = 4)
    private Integer anioConstruccion;

    @Column(name = "SUPERF_CONST", precision = 10, scale = 2)
    private BigDecimal superfConst;

    @Column(name = "UNIDAD_MEDIDA_ID", precision = 8)
    private Long unidadId;

    @ManyToOne
    @JoinColumn(name = "UNIDAD_MEDIDA_ID",insertable = false,updatable = false)
    private UnidadMedidaTerreno unidad;

    @Column(name = "IND_VIVIENDA_SOCIAL")
    private Integer indicadorViviendaSocial;

    @Column(name = "DFL2")
    private Boolean dfl2;

    @Column(name = "NUM_PROPIEDAD_DFL2", precision = 4)
    private Integer numPropiedadDFL2;

    @Column(name = "MONEDA_PACTADA_ID", precision = 8)
    private Long monedaPactadaId;

    @ManyToOne
    @JoinColumn(name = "MONEDA_PACTADA_ID",insertable = false,updatable = false)
    private Moneda monedaPactada;

    @Column(name = "VALOR_TASACION_UF", precision = 10, scale = 4)
    private BigDecimal valorTasacionuf;

    @Column(name = "SELLO_VERDE")
    private Boolean selloVerde;

    @Column(name = "VALOR_LIQUIDEZ_UF", precision = 10, scale = 4)
    private BigDecimal valorLiquidezuf;

    @Column(name = "MONTO_MIN_ASEGURABLE_UF", precision = 10, scale = 4)
    private BigDecimal montoMinAsegurableuf;

    @Column(name = "OBSERVACION_TASACION", length = 3000)
    private String observacionTasacion;

    @Column(name = "NUM_BIENRAIZ_INST")
    private String numBienraizInst;

    @Column(name = "NUM_GTIA_INST")
    private String numGtiaInst;

    @Column(name = "TIPO_GARANTIA_ID", length = 8)
    private Long tipoGarantiaId;

    @ManyToOne
    @JoinColumn(name = "TIPO_GARANTIA_ID",insertable = false,updatable = false)
    private TipoGarantia tipoGarantia;

    @Column(name = "AFAVORDE")
    private String afavorde;

    @Column(name = "ANNO_CERTIF_ASIG_ROLES", precision = 10, scale = 4)
    private BigDecimal annoCertifAsigRoles;

    @Column(name = "CERTIF_ASIG_ROLES")
    private String certifAsigRoles;

    @Column(name = "CERTIFICADO_NRO", precision = 9, scale = 2)
    private BigDecimal certificadoNro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_PERMISO_EDIF")
    private Date fechaPermisoEdif;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_RECEPC_FINAL")
    private Date fechaRecepcFinal;

    @Column(name = "MATRICERIA_HIPO")
    private Boolean matriceriaHipo;

    @Column(name = "MUNICIPALIDAD_ID", precision = 8)
    private Long municipalidadId;

    @ManyToOne
    @JoinColumn(name = "MUNICIPALIDAD_ID",insertable = false,updatable = false)
    private Comuna municipalidad;

    @Column(name = "NUM_CARATULA_CBR")
    private String numCaratulaCbr;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ESTIMADA_SALIDA_CBR")
    private Date fechaEstimadaSalidaCBR;

    @Column(name = "TOTAL_MONTO_UF_CRESG", precision = 10, scale = 4)
    private BigDecimal totalMontoUfCresg;

    @Column(name = "TOTAL_MONTO_UF_REPARO", precision = 10, scale = 4)
    private BigDecimal totalMontoUfReparo;

    @Column(name = "VALOR_TASACIONPESOS", precision = 14, scale = 2)
    private BigDecimal valorTasacionpesos;

    @Column(name = "SEGURO_ID", precision = 8)
    private Long seguroId;

    @ManyToOne
    @JoinColumn(name = "SEGURO_ID",updatable = false,insertable = false)
    private Seguro seguro;

    @Column(name = "USUARIO_ID" , precision = 8)
    private Long wfhUsuario;

    @OneToMany(mappedBy = "bienRaiz")
    @JsonIgnore
    private Set<CaBienRaiz> caBienRaiz;

    @Transient
    private String direccion;

}

package cl.api.bienraiz.models;

import cl.api.bienraiz.utils.Constant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the WFH_SEGUROS database table.
 */
@Entity
@Table(name = "WFH_SEGUROS", schema = Constant.DB_SCHEMA)
@JsonIgnoreProperties({"hibernateLazyInitializer", "entityClass"})
@Getter
@Setter
@NoArgsConstructor
public class Seguro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "GLOSA")
    private String glosa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANIA_SEGURO_ID")
    private CiaSeguro ciaSeguro;

    @Column(name = "POLIZA")
    private String poliza;

    //tasaUnificada
    @Column(name = "TASA")
    private BigDecimal tasa;

    @OneToOne
    @JoinColumn(name = "TIPO_SEGURO_ID", referencedColumnName = "ID")
    private TipoSeguro tipoSeguro;

    @Column(name = "VIGENTE")
    private Boolean vigente;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BANCO_ID", referencedColumnName = "ID")
    private Banco banco;

    @Column(name = "COBERTURA")
    private String cobertura;

    @Column(name = "COLECTIVO")
    private Boolean colectivo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DESDE")
    private Date desde;

    @Column(name = "EN_SIMULADOR")
    private Boolean enSimulador;

    @Column(name = "GLOSA_CORTA")
    private String glosaCorta;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HASTA")
    private Date hasta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSTITUCION_ID", referencedColumnName = "ID")
    private Institucion institucion;

    @Column(name = "NOMBRE_SIMULADOR")
    private String nombreSimulador;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERIODICIDAD_ID", referencedColumnName = "ID")
    private PeriodicidadPago periodicidad;

    @Column(name = "TASA_INCENDIO")
    private BigDecimal tasaIncendio;

    @Column(name = "TASA_SISMO")
    private BigDecimal tasaSismo;

    @Column(name = "TIPO_COBERTURA")
    private String tipoCobertura;

    //bi-directional many-to-one association to Intermediario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INTERMEDIARIO_ID")
    private Intermediario intermediario;


}


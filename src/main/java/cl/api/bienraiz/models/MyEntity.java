package cl.api.bienraiz.models;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.UUID;

/**
 * Esta clase nos permitirá agregar comportamiento generico a las entity
 *
 * @author Claudio Olivares
 */
class MyEntity implements Serializable {

    /***
     * Variables para brindar comportamiento extra a los entity al trabajarlos
     * en DataTable. Esta medida ya que no se puede utilizar el ID de la BD, ya
     * que no siempre el entity trabajado existirá en la BD.
     * */
    @Transient
    private String rowKey;
    
    @Transient
    private Boolean rowSelected;

    public MyEntity() {
        this.rowKey = "RK" + UUID.randomUUID();
        this.rowSelected = Boolean.FALSE;
    }

    /**
     * Getters & Setters
     */
    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public Boolean getRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(Boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}

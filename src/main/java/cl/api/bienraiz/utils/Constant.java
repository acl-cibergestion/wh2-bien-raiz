package cl.api.bienraiz.utils;

public class Constant {

    private Constant() {
    }

    public static final String DB_SCHEMA = "dbo";
    public static final String CONST_FOLDER_ID = "carpetaId";
}

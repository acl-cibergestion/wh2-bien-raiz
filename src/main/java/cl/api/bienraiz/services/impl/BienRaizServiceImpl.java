package cl.api.bienraiz.services.impl;

import cl.api.bienraiz.dto.BienRaizDto;
import cl.api.bienraiz.dto.ResponseDto;
import cl.api.bienraiz.error.ErrorCode;
import cl.api.bienraiz.especificaciones.SpecificationsBuilder;
import cl.api.bienraiz.exceptions.CustomException;
import cl.api.bienraiz.models.*;
import cl.api.bienraiz.repositories.*;
import cl.api.bienraiz.services.BienRaizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.*;

import static cl.api.bienraiz.utils.Constant.CONST_FOLDER_ID;


@Service("bienRaizService")
public class BienRaizServiceImpl implements BienRaizService {

    private final Logger logger = LoggerFactory.getLogger(BienRaizServiceImpl.class);

    @Autowired
    BienRaizRepositorio bienRaizRepositorio;

    @Autowired
    BrAdicionalRepositorio brAdicionalRepositorio;

    @Autowired
    BrDatoAlzamientoRepositorio brDatoAlzamientoRepositorio;

    @Autowired
    CaBienRaizRepositorio caBienRaizRepositorio;

    @Autowired
    CarpetaRepositorio carpetaRepositorio;

    @Autowired
    SpecificationsBuilder<BienRaiz> specificationsBuilder;

    Set<BrDatoAlzamiento> brDatoAlzamientoSet = new HashSet<>();

    Set<BrAdicional> brAdicionalSet = new HashSet<>();

    @Override
    @Transactional
    public boolean save(List<BienRaiz> bienRaizList, Long carpetaId, Long usuarioId) {
        boolean response;
        try {
            Optional<Carpeta> carpeta = carpetaRepositorio.findById(carpetaId);
            bienRaizList.stream().forEach(bienRaiz -> {
                bienRaiz.setWfhUsuario(usuarioId);
                this.brDatoAlzamientoSet = bienRaiz.getDatosAlzamiento();
                this.brAdicionalSet = bienRaiz.getBrAdicionales();
                bienRaiz.setDatosAlzamiento(null);
                bienRaiz.setBrAdicionales(null);
                var bRaiz = bienRaizRepositorio.save(bienRaiz);
                brDatoAlzamientoSet.stream().forEach(datoAlzamiento -> datoAlzamiento.setBienRaiz(bRaiz));
                brDatoAlzamientoRepositorio.saveAll(brDatoAlzamientoSet);
                brAdicionalSet.stream().forEach(datoAdicional -> datoAdicional.setBienRaiz(bRaiz));
                brAdicionalRepositorio.saveAll(brAdicionalSet);
                var caBienRaiz = new CaBienRaiz();
                caBienRaiz.setBienRaiz(bRaiz);
                if (carpeta.isPresent()) {
                    caBienRaiz.setCarpeta(carpeta.get());
                }
                caBienRaizRepositorio.save(caBienRaiz);
            });

            response = true;
        } catch (Exception e) {
            logger.error("Error al guardar los bien raiz :", e);
            response = false;
        }

        return response;
    }

    @Override
    public List<BienRaiz> findBienRaiz(Map<String, Map<String, String>> request) throws CustomException {
        try {
            List<BienRaiz> bienRaizList = new ArrayList<>();
            Map<String, String> filters = request.get("filters");
            Specification<CaBienRaiz> specification = (root, query, builder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get("carpeta").get("id"), filters.get("carpetaId")));
                return builder.and(predicates.toArray(new Predicate[0]));
            };
            var caBienRaiz = caBienRaizRepositorio.findAll(specification);
            for (CaBienRaiz caBienRaiz1 : caBienRaiz) {
                bienRaizList.add(caBienRaiz1.getBienRaiz());
            }
            return bienRaizList;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    @Transactional
    public boolean deleteBienRaizCarpeta(Long id) {
        boolean response;
        try {
            Specification<BienRaiz> specification = Specification.where(specificationsBuilder.withParameterOfAnotherEntity(
                    id, "caBienRaiz", "carpeta"));
            List<BienRaiz> bienRaizList = bienRaizRepositorio.findAll(specification);
            Optional<Carpeta> carpeta = carpetaRepositorio.findById(id);
            for (BienRaiz bienRaiz : bienRaizList) {
                brAdicionalRepositorio.deleteByBienRaiz(bienRaiz);
                brDatoAlzamientoRepositorio.deleteByBienRaiz(bienRaiz);
                if (carpeta.isPresent()) {
                    caBienRaizRepositorio.deleteByCarpetaAndBienRaiz(carpeta.get(), bienRaiz);
                }
                bienRaizRepositorio.deleteById(bienRaiz.getId());
            }
            response = true;
        } catch (Exception e) {
            logger.error("Error al eliminar el bien raiz :", e);
            response = false;
        }
        return response;

    }

    private void deleteBienRaiz(BienRaiz bienRaiz, Long carpetaId) {
        try {
            brAdicionalRepositorio.deleteByBienRaiz(bienRaiz);
            brDatoAlzamientoRepositorio.deleteByBienRaiz(bienRaiz);
            Optional<Carpeta> carpeta = carpetaRepositorio.findById(carpetaId);
            if (carpeta.isPresent()) {
                caBienRaizRepositorio.deleteByCarpetaAndBienRaiz(carpeta.get(), bienRaiz);
            }
            bienRaizRepositorio.deleteById(bienRaiz.getId());
        } catch (Exception e) {
            logger.error("Error al eliminar el bien raiz :", e);

        }
    }

    @Override
    @Transactional
    public boolean edit(List<BienRaiz> bienRaizList, Long carpetaId, Long usuarioId) {
        var response = false;
        try {
            logger.info("EDIT");
            var valueFilters = new HashMap<String, String>();
            valueFilters.put(CONST_FOLDER_ID, carpetaId.toString());
            Map<String, Map<String, String>> filters = new HashMap<>();
            filters.put("filters", valueFilters);
            var bienYRaicesListBd = findBienRaiz(filters);
            for (BienRaiz bienRaiz : bienRaizList) {
                if (bienRaiz.getId() != null) {
                    var bienyRaizEdit = bienYRaicesListBd.stream()
                            .filter(bienRaiz1 -> bienRaiz1.getId().equals(bienRaiz.getId()))
                            .findAny()
                            .orElse(null);
                    if (bienyRaizEdit != null) {
                        editBienYRaiz(bienRaiz, carpetaId, usuarioId);
                        bienYRaicesListBd.remove(bienyRaizEdit);
                    } else {
                        return false;
                    }
                } else {
                    editBienYRaiz(bienRaiz, carpetaId, usuarioId);
                }
            }
            bienYRaicesListBd.forEach(bienRaiz ->
            {
                try {
                    deleteBienRaiz(bienRaiz, carpetaId);
                } catch (Exception e) {
                    logger.error(String.format("Error al eliminar bienRaiz: %s", e.getMessage()));
                }
            });
            logger.info("EDIT Finalizado return true");
            response = true;
        } catch (Exception e) {
            logger.error("Error al actualizar el bien raiz :", e);
        }
        return response;
    }

    void editBienYRaiz(BienRaiz bienRaiz, Long carpetaId, Long usuarioId) throws CustomException {
        try {
            logger.info("-->editBienYRaiz");
            Optional<Carpeta> carpeta = carpetaRepositorio.findById(carpetaId);
            bienRaiz.setWfhUsuario(usuarioId);
            Set<BrDatoAlzamiento> brDatoAlzamiento = bienRaiz.getDatosAlzamiento();
            Set<BrAdicional> brAdicional = bienRaiz.getBrAdicionales();
            bienRaiz.setDatosAlzamiento(null);
            bienRaiz.setBrAdicionales(null);
            var bRaiz = bienRaizRepositorio.save(bienRaiz);
            editBrDatoAlzamiento(bRaiz, brDatoAlzamiento);
            editBrAdicionales(bRaiz, brAdicional);
            if(carpeta.isPresent()) {
                var caBienRaizBd = caBienRaizRepositorio.findByBienRaizAndAndCarpeta(bRaiz, carpeta.get());
                if (!caBienRaizBd.isPresent()) {
                    var caBienRaiz = new CaBienRaiz();
                    caBienRaiz.setBienRaiz(bRaiz);
                    carpeta.ifPresent(caBienRaiz::setCarpeta);
                    caBienRaizRepositorio.save(caBienRaiz);
                }
            }
            logger.info("<--editBienYRaiz");
        } catch (Exception e) {
            logger.error("Error al actualizar el bien raiz :", e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    void editBrAdicionales(BienRaiz bienRaiz, Set<BrAdicional> brAdicionales) {
        var brAdicionalesBd = brAdicionalRepositorio.findAllByBienRaiz(bienRaiz);
        this.brAdicionalSet = brAdicionales;
        brAdicionalSet.forEach(item -> {
            item.setBienRaiz(bienRaiz);
            if (item.getId() != null) {
                var brAdicionalesEdit = brAdicionalesBd.stream()
                        .filter(brAdicionales1 -> brAdicionales1.getId().equals(item.getId()))
                        .findAny()
                        .orElse(null);
                if (brAdicionalesEdit != null) {
                    item.setBienRaiz(brAdicionalesEdit.getBienRaiz());
                }
                brAdicionalRepositorio.save(item);
                brAdicionalesBd.remove(brAdicionalesEdit);
            } else {
                brAdicionalRepositorio.save(item);
            }
        });
        brAdicionalesBd.forEach(brAdicional ->
        {
            try {
                brAdicionalRepositorio.deleteById(brAdicional.getId());
            } catch (Exception e) {
                logger.error(String.format("Error al eliminar brAdicional: %s", e.getMessage()));
            }
        });
    }

    void editBrDatoAlzamiento(BienRaiz bienRaiz, Set<BrDatoAlzamiento> datosAlzamiento) {
        var brDatoAlzamientoBd = brDatoAlzamientoRepositorio.findAllByBienRaiz(bienRaiz);
        this.brDatoAlzamientoSet = datosAlzamiento;
        brDatoAlzamientoSet.forEach(item -> {
            item.setBienRaiz(bienRaiz);
            if (item.getId() != null) {
                var brDatoAlzamientoEdit = brDatoAlzamientoBd.stream()
                        .filter(brDatoAlzamiento1 -> brDatoAlzamiento1.getId().equals(item.getId()))
                        .findAny()
                        .orElse(null);
                if (brDatoAlzamientoEdit != null) {
                    item.setBienRaiz(brDatoAlzamientoEdit.getBienRaiz());
                }
                brDatoAlzamientoRepositorio.save(item);
                brDatoAlzamientoBd.remove(brDatoAlzamientoEdit);
            } else {
                brDatoAlzamientoRepositorio.save(item);
            }
        });
        brDatoAlzamientoBd.forEach(brDatoAlzamiento ->
        {
            try {
                brDatoAlzamientoRepositorio.deleteById(brDatoAlzamiento.getId());
            } catch (Exception e) {
                logger.error(String.format("Error al eliminar participante: %s", e.getMessage()));
            }
        });
    }

    @Override
    public ResponseDto findByFolder(Map<String, Map<String, String>> request) throws CustomException {
        List<BienRaizDto> bienRaizs = new ArrayList<>();
        int estacionamientos;
        int bodegas;
        List<BienRaiz> bienRaizList = findBienRaiz(request);
        Set<PjProyecto>
                pjProyectos = new HashSet<>();
        bienRaizList.forEach(bienRaiz -> {
            if (Objects.nonNull(bienRaiz.getPjProyecto())) {
                pjProyectos.add(bienRaiz.getPjProyecto());
            }
        });
        for(BienRaiz bienRaiz : bienRaizList){
            var bienRaizDto = new BienRaizDto();
            estacionamientos = 0;
            bodegas = 0;
            for(BrAdicional brAdicional : bienRaiz.getBrAdicionales()){
                if(brAdicional.getTipoAdicional()==1){
                    bodegas++;
                } else {
                    estacionamientos++;
                }
            }
            BeanUtils.copyProperties(bienRaiz, bienRaizDto);
            bienRaizDto.setBodegas(bodegas);
            bienRaizDto.setEstacionamientos(estacionamientos);
            bienRaizs.add(bienRaizDto);
        }
        var responseDto = new ResponseDto();
        responseDto.setBienRaiz(bienRaizs);
        responseDto.setProyecto(pjProyectos);
        return responseDto;
    }
}

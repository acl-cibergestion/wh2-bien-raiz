package cl.api.bienraiz.services;


import cl.api.bienraiz.dto.ResponseDto;
import cl.api.bienraiz.exceptions.CustomException;
import cl.api.bienraiz.models.BienRaiz;

import java.util.List;
import java.util.Map;


public interface BienRaizService {

    boolean save(List<BienRaiz> bienRaizList, Long carpetaId, Long usuarioId);

    List<BienRaiz> findBienRaiz(Map<String, Map<String, String>> request) throws CustomException;

    boolean deleteBienRaizCarpeta(Long carpetaId);

    boolean edit(List<BienRaiz>bienRaizList, Long carpetaId, Long usuarioId);

    ResponseDto findByFolder(Map<String, Map<String, String>> request) throws CustomException;
}
